package com.example.routers

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete

trait HomeRoute {
    // we leave these abstract, since they will be provided by the App
    implicit def system: ActorSystem

    //lazy val log = Logging(system, classOf[UserRoutes])

    lazy val homeRoutes: Route =
      concat(
        //home page
        pathPrefix("" | ""./) {
          pathEnd {
            get {
              complete("Thank you for visiting our home page")
            }
          }
        },
      )
}
