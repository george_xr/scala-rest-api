package com.example.routers

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.{delete, get, post}
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path

import scala.concurrent.Future
import com.example.config.JsonSupport
import com.example.repo._
import org.slf4j.{Logger, LoggerFactory}
import com.google.inject.Inject

import scala.util.{Failure, Success}


//#user-routes-class
class UserRoutes @Inject()(userComponent: UserComponent) extends JsonSupport {
  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  lazy val userRoutes: Route =
    concat(
        //#users-pages
      pathPrefix("users") {
        concat(
          pathEnd {
            concat(
              // get all users
              get {
                val users: Future[UserClasses] = userComponent.getAllUsers
                onComplete(users){
                  case Success(result) => result match {
                    case result: Users => complete(result)
                    case result: ActionPerformed => complete(result)
                    case _ => complete(ActionPerformed("Something went wrong " + result))
                  }
                  case Failure(ex) => complete(ActionPerformed("Error when getting all user" + ex))
                }
              },
              // user create
              post {
                entity(as[User]) { user =>
                  val userCreated: Future[ActionPerformed] = userComponent.createUser(user)
                  onComplete(userCreated){
                    case Success(result) => complete((StatusCodes.Created, result))
                    case Failure(ex) => complete(ActionPerformed("Error when created user" + ex))
                  }
                }
              },
              // user update
              put {
                entity(as[User]) { user =>
                  val userUpdate: Future[ActionPerformed] = userComponent.updateUser(user)
                  onComplete(userUpdate){
                    case Success(result) => result match {
                      case result: ActionPerformed => complete((StatusCodes.Created, result))
                      case _ => complete(ActionPerformed("Something went wrong " + result))
                    }
                    case Failure(ex) => complete(ActionPerformed("Error when updated user " + ex))
                  }
                }
              }
            )
          },
          //#user-get #user-delete
          path(Segment) { name =>
            concat(
              // get user by name
              get {
                val maybeUser: Future[UserClasses] = userComponent.getUserByName(name)
                onComplete(maybeUser) {
                  case Success(user) => user match {
                    case user: ActionPerformed => complete(ActionPerformed(s"User with name $name not found"))
                    case user: User => complete(user)
                    case _ => complete(ActionPerformed("Something went wrong " + user))
                  }
                  case Failure(ex) => complete(ActionPerformed(s"Exception for user get by name $ex"))
                }
              },
              // delete user by name
              delete {
                val userDeleted: Future[ActionPerformed] = userComponent.deleteUser(name).mapTo[ActionPerformed]
                onComplete(userDeleted){
                  case Success(value) => complete((StatusCodes.OK, value))
                  case Failure(ex) => complete(ActionPerformed(s"Error for user delete $ex"))
                }
              }
            )
          }
        )
      }
  )
}
