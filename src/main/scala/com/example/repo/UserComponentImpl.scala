package com.example.repo

import com.example.connection.DBComponent
//import slick.jdbc.H2Profile.api._

import scala.concurrent.Future
import slick.lifted

import scala.concurrent.ExecutionContext.Implicits.global
import com.google.inject.Inject
import org.slf4j.{Logger, LoggerFactory}

trait UserClasses
final case class User(name: String, age: Int, countryOfResidence: String, id: Option[Int]) extends UserClasses
final case class Users (users: Seq[User]) extends UserClasses
final case class ActionPerformed(description: String) extends UserClasses

trait UserComponent{
  def getAllUsers: Future[UserClasses]
  def createUser(user: User): Future[ActionPerformed]
  def getUserByName(name: String): Future[UserClasses]
  def deleteUser(name: String): Future[ActionPerformed]
  def updateUser(user: User): Future[ActionPerformed]
  def createTable: Future[_]
}

class UserComponentImpl @Inject()(db: DBComponent) extends UserComponent {

  // privat db to pablic dbPablic for import driver.api._
  val dbPublic: DBComponent = db
  import dbPublic.driver.api._

  implicit def DBComponentToDatabase(x: DBComponent): Database = x.db

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  protected val users = lifted.TableQuery[UsersTabImpl]

  // crud methods
  def getAllUsers: Future[UserClasses] = db.run(users.result)
    .map(seqUser =>
      if(seqUser.isEmpty) ActionPerformed("Users not found")
      else Users(seqUser)
    )

  def createUser(user: User): Future[ActionPerformed] =
    db.run(users.filter(_.name === user.name).exists.result) // check user
      .flatMap {
        case true => Future {ActionPerformed("User exsist")}
        case false => db.run(users += user).map(_ => ActionPerformed(s"User created ${user.name}"))
      }


  def getUserByName(name: String): Future[UserClasses] = db.run(users.filter(_.name === name).result)
    .map(seqUsers =>
      if(seqUsers.isEmpty) ActionPerformed("User does't exsist")
      else seqUsers.head
    )

  def deleteUser(name: String): Future[ActionPerformed] =
    db.run(users.filter(_.name === name).exists.result) // check user
      .flatMap {
        case false => Future {ActionPerformed("User does not exist ")}
        case true => db.run(users.filter(_.name === name).delete).map(_ => ActionPerformed(s"User $name deleted. "))
      }


  def updateUser(user: User): Future[ActionPerformed] =
    db.run (users.filter (_.name === user.name).exists.result) // check user
    .flatMap {
      case false => Future{ActionPerformed ("User does not exist")}
      case true => db.run (users.filter (_.name === user.name)
        .map (m => (m.age, m.country) )
        .update (user.age, user.countryOfResidence) )
      .map (_ => ActionPerformed (s"User ${user.name} updated.") )
    }


  //create table and zero user
  def createTable: Future[_] = db.run(DBIO.seq(users.schema.create)//, users += defaultsUser)
      .map(_ => logger.info("Create table users Successfully ")))

  // user schema
  private[repo] class UsersTabImpl(tag: Tag) extends Table[User](tag, "USERS") {
    def id = column[Int]("USER_ID", O.PrimaryKey, O.AutoInc) // This is the primary key column
    def name = column[String]("USER_NAME")

    def age = column[Int]("AGE")

    def country = column[String]("COUNTRY")

    def * = (name, age, country, id.?) <> (User.tupled, User.unapply)
  }

}
