package com.example.connection

import org.slf4j.{Logger, LoggerFactory}

class H2DBComponent extends DBComponent {

  val logger: Logger = LoggerFactory.getLogger(this.getClass)

  val driver = slick.jdbc.H2Profile

  import driver.api._

  val db: Database = Database.forConfig("h2mem1")

}
