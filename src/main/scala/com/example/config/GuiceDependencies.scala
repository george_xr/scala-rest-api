package com.example.config

import com.example.connection.{DBComponent, H2DBComponent}
import com.example.repo.{UserComponent, UserComponentImpl}
import com.google.inject.AbstractModule
import net.codingwell.scalaguice.ScalaModule

class GuiceDependencies extends AbstractModule with ScalaModule {
  override def configure(): Unit ={
    bind[DBComponent] to classOf[H2DBComponent]
    bind[UserComponent] to classOf[UserComponentImpl]
  }
}
