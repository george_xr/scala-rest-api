package com.example

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.example.repo._
import com.example.routers._
import akka.http.scaladsl.server.Directives._
import com.example.config.GuiceDependencies
import com.google.inject.Guice
import org.slf4j.LoggerFactory
import net.codingwell.scalaguice.InjectorExtensions._


object QuickstartServer extends App  with HomeRoute {

  implicit val system: ActorSystem = ActorSystem("helloAkkaHttpServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  val logger = LoggerFactory.getLogger(this.getClass)


  private val injector = Guice.createInjector(new GuiceDependencies())
  // DI for UserRouts from UserComponent
  val guiceUserRoute: UserRoutes = injector.instance[UserRoutes]
  // DI for UserComponent from DBComponent
  val guiceUserComponent = injector.instance[UserComponent]

  //create User table and zero user
  guiceUserComponent.createTable

  lazy val routes: Route = concat(guiceUserRoute.userRoutes, homeRoutes)

  val serverBinding: Future[Http.ServerBinding] = Http().bindAndHandle(routes, "0.0.0.0", 8080)

  serverBinding.onComplete {
    case Success(bound) =>
      println(s"Server online at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}/")
    case Failure(e) =>
      Console.err.println(s"Server could not start!")
      e.printStackTrace()
      system.terminate()
  }

  Await.result(system.whenTerminated, Duration.Inf)
}
